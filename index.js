//khai báo thư viện express
const express = require('express');
const { courseRouter } = require('./app/routes/courseRouter');
const { reviewRouter } = require('./app/routes/reviewRouter');

//khai báo app
const app = express();

//khai báo cổng
const port = 8000;



app.use(`/`, courseRouter);
app.use(`/`,reviewRouter);



//khởi động app
app.listen(port,()=>{
    console.log(`app listening on port ${port}`);
})