//khai báo thư viện express
const express = require("express");

//khai báo middleware
const {
        getAllCoursesMiddleware,
        getCoursesMiddleware,
        postCoursesMiddleware,
        putCoursesMiddleware,
        deleteCoursesMiddleware   
} = require('../middlewares/courseMiddleware');

//tạo ra router
const courseRouter = express.Router();


courseRouter.get('/courses',getAllCoursesMiddleware,(req,res) =>{
    console.log(req.method);
    res.json({
        message : "Get all course"
    })
})

courseRouter.get('/courses/:courseId', getCoursesMiddleware,(req,res)=>{
    let courseId = req.params.courseId;
    res.json({
        message : `Get course Id = ${courseId}`
    })
})

courseRouter.post('/courses/', postCoursesMiddleware,(req,res)=>{
    res.json({
        message : `Create a new Course`
    })
})

courseRouter.put('/courses/:courseId', putCoursesMiddleware,(req,res)=>{
    let courseId = req.params.courseId;
    res.json({
        message : `Update Course with id : ${courseId}`
    })
})

courseRouter.delete('/courses/:courseId', deleteCoursesMiddleware,(req,res)=>{
    let courseId = req.params.courseId;
    res.json({
        message : `Delete Course with id : ${courseId}`
    })
})

module.exports = {courseRouter}